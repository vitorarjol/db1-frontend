import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/views/Login'
import Register from '@/views/Register'
import Home from '@/views/Home'
import Tags from '@/views/Tags'
import Categories from '@/views/Categories'
import AllPosts from '@/views/posts/Index'
import AddPost from '@/views/posts/New'
import EditPost from '@/views/posts/Edit'
import ShowPost from '@/views/posts/Show'

Vue.use(Router)

function guardRoute (to, from, next) {
  let token = ''
  token = localStorage.getItem('db1-token')
  if (token && token.length > 0) {
    next()
  } else {
    next('/login')
  }
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/posts',
      name: 'posts',
      component: AllPosts,
      beforeEnter: guardRoute
    },
    {
      path: '/posts/:postId',
      name: 'view-post',
      component: ShowPost
    },
    {
      path: '/posts/adicionar',
      name: 'new-post',
      component: AddPost,
      beforeEnter: guardRoute
    },
    {
      path: '/posts/:postId/editar',
      name: 'edit-post',
      component: EditPost,
      beforeEnter: guardRoute
    },
    {
      path: '/tags',
      name: 'tags',
      component: Tags,
      beforeEnter: guardRoute
    },
    {
      path: '/categorias',
      name: 'categories',
      component: Categories,
      beforeEnter: guardRoute
    }
  ]
})
