# db1-frontend

> Single Page Application desenvolvido com Vue.js

## Passo a passo para utilização

``` bash
# instalação das dependências
npm install

# servir com hot reload habilitado no localhost:8080
npm run dev

# build para produção
npm run build

```
